<div dir="rtl" align="right" style="color:blue"><h1>بنام خدا</h1></div>

###### top

- [Make Container from LiveOS](#make-container-from-liveos)
- [Send Command to Container](#send-command-to-container)
- [Some Regulare Code](#some-regulare-code)
- [Linking](#linking)
- [Rmove,Delete](#rmovedelete)
- [Run,Attach](#runattach)
- [Ps](#ps)
- [Commit,Export,Import,Save](#commitexportimportsave)
- [Resolving Some Errors](#resolving-some-errors)
- [Save](#save)


[top](#top)
### Make Container from LiveOS

<div dir="rtl" align="right">چگونه از یک لینوکس ردهت درحال اجرا یک داکر بسازیم نسخه ۱.۱۰.۳</div>
- how to make docker image of running linux redhat OS:

```bash
   tar --numeric-owner --exclude=/proc --exclude=/sys --exclude=/mnt --exclude=/var/cache \
       --exclude=/usr/share/doc --exclude=/tmp --exclude=/var/log -zcvf /mnt/RHEL7.2-base.tar.gz /
   cat /mnt/RHEL7.2-base.tar.gz | docker import - RHEL7.2/7.2
   docker run -i -t RHEL7.2/7.2 /bin/bash
```

[top](#top)       
### Send Command to Container
- run ssh

```bash
   yum install openssh-server openssh openssh-client -y --nogpgcheck
   /usr/sbin/sshd-keygen
   /usr/sbin/sshd
```
- send command to running container

```bash
   docker exec `docker ps ID` Command
   docker exec -it `docker ps ID` /bin/bash <- connet to running container
```
- connect to running container with the same Output Terminal

```bash
  docker run --name ContainerName
  docker attach `docker ps ID Or ContainerName`
```
- how to copy a file to running container

```bash
   docker cp Files `docker images name`:Path/FileNam
```
[top](#top)       
### Some Regulare Code
- some commands

```vim
   service docker start
   docker images
   docker ps
   docker ps -a --> it gaves some report of containers may you run and exit them but they still busy.
   docker stop ContainerID
   docker kill ContainerID
   docker rm ContainerID
   docker cp /Files to Copy 149805c9921d:/root/
   docker commit 149805c9921d docker-couchbase/01
   docker images
   docker run -h HostName -i -t docker-Image /bin/bash
   docker commit 4619e3893ed5 DockerName/No
   docker run -p HostPort:ContainerPort -it ...
   docker export ContainerID -o ExportContainerName
   docker tag ContainerID ContainerName <- after Import container, New Container has None Name
   docker run -v /path/to/host:/path/to/container ...
   
   umount /var/lib/docker/devicemapper/mnt/656cfd09aee399c8ae8c8d3e7...
```

[top](#top)       
### Linking
- Link a container and some issues:
   - Howto Named a container : `docker run -d --name NewName ContainerName`.
   - Howto Linked : `docker run -d --link "AboveName" ...`.
- Install some packages into container:
  - pip3:

    ```bash
      wget https://raw.githubusercontent.com/pypa/pip/701a80f451a62aadf4eeb21f371b45424821582b/contrib/get-pip.py -O /root/get-pip.py
      python3.4 /root/get-pip.py
    ```
[top](#top)       
### Rmove,Delete
- remove images:

```vim
  docker rmi -f ContainerIDs
```
   - may you faced issue like _docker: Error response from daemon: devmapper: Thin Pool has 142989 free data blocks \
      which is less than minimum required 163840 free data blocks. Create more free space in thin pool or use \
      dm.min_free_space option to change behavior._ then you should try:
   ```go
     docker rm $(docker ps -q -f status=exited)
     docker volume rm $(docker volume ls -qf dangling=true)
     docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
   ```
- Purne:

```vim
  docker system prune
```
- Autoremove container when you exit it:

```vim
  docker run -it --rm 
```


[top](#top)       
### Run,Attach
- keep alive container every time

```vim
  docker run --restart=always ...
  docker run --restart=on-failure:10 ... <- restart maximum 10 times on failure state
```
- Use Special Network:

```vim
  docker run --net=host ...  <- Host
  docker run --net=container:<name|id> ... <- Use Container Network
```

[top](#top)
### Resolving Some Errors:

1- [Docker “ERROR: could not find an available, non-overlapping IPv4 address pool among the defaults \
   to assign to the network”](https://stackoverflow.com/questions/43720339/docker-error-could-not-find-an-available-non-overlapping-ipv4-address-pool-am)
```vim
  docker network ls
  docker network rm $(docker network ls | grep -v "bridge" | awk '/ / { print $1 }')
```
2- [Unable to create docker0 if VPN is active · Issue #779 · docker/libnetwork](https://github.com/docker/libnetwork/issues/779#issuecomment-231727303)
```vim
   openvpn --config vpn_config_file --route-up fix-routes.sh
```
   - or script like this:

   ```vim
      #!/bin/sh
      echo "Adding default route to $route_vpn_gateway with /0 mask..."
      ip route add default via $route_vpn_gateway
      echo "Removing /1 routes..."
      ip route del 0.0.0.0/1 via $route_vpn_gateway
      ip route del 128.0.0.0/1 via $route_vpn_gateway
   ```



[top](#top)

# Docker File Sample

## Docker Build file sample

docker file used for setup simple and related _container_ toghather

```elixir
FROM ImageName1:Tags1 as img1	<-- call image from DockerHub
LABEL "Key"="Value"				<-- can list by labels:docker images -f "key=Value"
WORKDIR "/app" 					<-- Where into image would you like to work
COPY SomeThingsfromLocal ./		<-- copies to WORKDIR
RUN npm install 				<-- Run used while create Image
EVN PG=9.3
RUN yum install postgres-$PG
ENV PATH=$PATH:/usr/postgres-$PG
VOLUME vol1						<-- create in /var/lib/docker/volumes and access with docker volumes ls
CMD ["npm","run","dev"]			<-- fire when you lunched image,Only one CMD Possible in dockerfile

FROM ImageName2:Tags2
COPY --FROM img1 /app/somethings /path/2/Img2/	<-- fantastic,We could catch previos image build data
ENTRYPOINT ["Command"]
CMD ["if use EntryPoint all things here would be parameter as above"]
EXPOSE portNo
```

then run `docker build .`



## Docker Compose Sample

**Docker Compose** used to setup _Ecosystem_ of _Containers_ in one Machine

```elixir
varsion: '3'
services:
	postgres:
		image: postgres:latest
	redis:
		image: ...
	rubyserver:
		build:
			context: /path
			dockerfile: dockerFileName
			volume:
				- /app/module				<-- bookmark this path to prevent of overwriting
				- /app:/app
			environment:
				- REDIS_HOST=redis			<-- this name exist in this compose file
				- REDIS_PORT=...
	nginx:
		build:
			...
		restart: always
		ports:
			- '8080:80'
```

`docker-compose up --build`



# Kubernetes

- used to deploy _containers_ on many machines called **Node** and managed _multiple differences containers_ on multiple machines
- there are two type commands:
  - minikube:
    this tool used for creating _kuberneties_ on local machine
  - kubctl:
    this tool used for manage **whole of Kubernet Clusters**

![KuberModel1](Meta/KuberModel1.png)

- about above Pic:
  - each **Node** is a Machine that contains _multiple/single Container_
  - **Masters** are machines those _Manages_ Nodes
  - **Master** decides _which Process(container)_ run on _which Node_
- there two types of coding files to send them to Kubernet:
  - imparetive: **Kubernet System** decides how to run our Cluster
  - declarative: we manage **Kubernet** to run our order



![MainKubernetes](Meta/MainKubernetes.png)

to setup above Ecosystem

```elixir
# client-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
	name: client-deployment
	spec:
		replicas: 3
		selector:
			matchLabels:
				component:client		<--this structure shold exist in labels and docker-compose
		template:
			metadata:
				labels:
					component:client
			spec:
				containers:
					- name: client
					 image: someImageFromHub
					 ports:
					 	- containerPort: 3000
					 	
# client-cluster-ip-service.yaml
apiVersion: apps/v1
kinds: Service
metadata:
	name: client-cluster-ip-service
spec:
	type: ClusterIP
	selector:
		component: client
	ports:
		- port: 3000
		  targetPort: 3000				<-- there is't force to be the same, but prefered
```

```elixir
# server-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
	name: server-deployment
spec:
	replicas: 3
	selector:
		matchLabels:
			component: server
	template:
		metadata:
			labels:
				component: server
		spec:
			container:
				- name: server
				  image: something
				  ports:
				  	- containerPort: 5000
				  	
# server-cluster-ip.yaml
apiVersion: apps/v1
kind: Service
metadata:
	name: server-cluster-ip.yaml
spec:
	type: ClusterIP
	selector:
		component: server
	ports:
		- port: 5000
		  targetPort: 5000
```

```elixir
# worker-deployment.yaml
apiVersion: apps/v1
spec:
	replicas: 1
	selector:
		metadata:
	...
	
```

```elixir
# redis-deployment.yaml
api...
kind: Deployment
name: redis-deployment
replicas: 1
component: redis
...
containerPort: 6379

# redis-cluster-ip.yaml
kind: Service
type: ClusterIP
component: redis
ports:
	- port: 6379
	  targetIP: 6379
```

- some tips:
  - if we change _configuration file_ setup, **Kubernet deploymanegment** effected them to cluster
  - if we change _image_ **Kubernet**wouldnot and couldnot understand them, therefor _we use Tag in Image names_.

[top](#top)

# Save

## Save an Image

## Save Container
to save a container as Image, we need go through these steps:
  1. run an Image and modify the container
  2. run `commit` command:
  ```bash
    docker commit ID|ContainerName New_Image_Name
  ```
  3. so we can delete the main Image
  4.  with `docker inspect ImageName`, all exist parameters will list






