<div align="right" dir="rtl" style="color:blue">بنام خدا</div>




[toc]

# Vagrant

[guide to install](https://gist.github.com/PaulNeumann/81f299a7980f0b74cec9c5cc0508172b)



## Config for KVM

should below plugin installed

```bash
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-mutate
```

after we could start like below

```elixir
vagrant init generic/ubuntu2004
vagrant up --provider=libvirt
```

to find boxes consider [this](https://app.vagrantup.com/boxes/search?provider=libvirt&q=&sort=downloads&utf8=%E2%9C%93)

![image-20220330161805298](Vagrant.assets/image-20220330161805298.png)



## common codes

thnkas to [wpscholar](https://gist.github.com/wpscholar)

```haskell
vagrant init <boxpath>
vagrant up 
vagrant resume <-- resume a suspended machine (vagrant up works just fine for this as well)
vagrant provision <-- forces reprovisioning of the vagrant machine
vagrant reload <-- restarts vagrant machine, loads new Vagrantfile configuration
vagrant reload <--provision
vagrant ssh <boxname> <-- go into Box
vagrant halt -- stops the vagrant machine
vagrant suspend -- suspends a virtual machine (remembers state)
vagrant destroy -- stops and deletes all traces of the vagrant machine
vagrant destroy -f -- same as above, without confirmation
vagrant box list -- see a list of all installed boxes on your computer
vagrant box add <name> <url> -- download a box image to your computer
vagrant box outdated -- check for updates vagrant box update
vagrant boxes remove <name> -- deletes a box from the machine
vagrant package -- packages a running virtualbox env in a reusable box
vagrant -v -- get the vagrant version
vagrant status -- outputs status of the vagrant machine
vagrant global-status -- outputs status of all vagrant machines
vagrant global-status --prune -- same as above, but prunes invalid entries
vagrant provision --debug -- use the debug flag to increase the verbosity of the output
vagrant push -- yes, vagrant can be configured to deploy code!
vagrant up --provision | tee provision.log -- Runs vagrant up, forces provisioning and logs all output to a file
```



## Vagrant Config file tips

the started of block is:

```ruby
Vagrant.configure("2") do |config|
```



### Naming

```ruby
config.vm.hostname="HostName" <-- we could use it for ssh
config.vm.define="defineName" <-- it shows Machine Name in the Host as: ParentFolderName_defineName_Timestamp
config.vm.provider "libvirt" |lvb| do
  lvb.name= "MachimeName"			<-- it shows Machine Name in Host as is
end
```

- if there are more than one machine in one _VagrantFile_ use below:

  ```ruby
  config.vm.define "MachineName" do |vm|
    ...
  end
  ```

  

### Network

#### Port forwarding

```ruby
config.vm.network "forwarded_port", guest:80, host:8080
```

#### Netwotk Type

```ruby
config.vm.network "private_network", ip: "96.110.113.10"
config.vm.network "public_network"
```



#### find VM Ip from host

1. run `vagrant global-status`
2. find related machine _id_
3. run `vagrant ssh-config id`



### Volumes

there are more than 25 **K8S Storage Types**.

#### Socket Volume

```haskell
apiVersion: v1
kind: Pod
metadata:
  name: docker
spec:
  containers:
  - name: docker
    image: docker:17.11
    command: ["sleep"]
    args: ["100000"]
    volumeMounts:
    - mountPath: /var/run/docker.sock
      name: docker-socket
  volumes:
  - name: docker-socket
    hostPath:
      path: /var/run/docker.sock
      type: Socket
```



### Shared Folder

#### Custom Shared Folder

```ruby
config.vm.synced_folder "/Host/Path","Vm/Path", type: "rsync"
```

and run `nohup vagrant rsync VMName &`



### Resources

you could config and set RAM and CPU

#### RAM,CPU

```ruby
config.vm.provider "libvirt" do |Vm|
  Vm.memory=1024
  Vm.cpus=1
end
```

##### CPU with libvirt provider

```ruby
config.vm.provider "libvirt" do |res|                            
  res.memory=2048                                                
  res.cpus=2                                                     
  res.driver = "kvm"                                             
	res.cputopology :sockets => '1', :cores => '2', :threads => '1'
	res.cpu_mode = 'host-passthrough'                              
end                                                              

```



### Provisioning



#### Provision by Shell

```ruby
config.vm.provision "shell", inline:<<-SHELL
	apt update -y
	apt install -y apache2
	service apache start
SHELL
```



### Lopping in config file

for create loop in config file we could use below syntax:

```ruby
Vagrant.configure("2") do |config|
  1.upto10 do |VmId|
    config.vm.define "vm-#{VmId}" do |Vm|
      Vm.vm.box = "ubuntu/focal64"
      Vm.vm.network "private_network", ip: 96.110.113.1#{VmId}
      ...
```
