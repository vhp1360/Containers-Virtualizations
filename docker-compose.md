<div dir="rtl" align="right">بنام خدا</div>

- [services](#services)
  - [blkio_config](#blkio_config)
  - [command](#command)
  - [config](#config)
  - [profiles](#profiles)
  - [depends_on](#depends_on)
  - [deploy](#deploy)
    - [endpoint_mode](#endpoint_mode)
    - [mode](#mode)
  - [restart](#restart)
  - [placement](#placement)
    - [constraints](#constraints)
    - [preferences](#preferences)
  - [resources](#resources)
  - [restart_policy](#restart_policy)
  - [rollback_config](#rollback_config)
  - [update_config](#update_config)
  - [devices](#devices)
  - [dns & dns_search](#dns & dns_search)
  - [entrypoint](#entrypoint)
  - [env_file](#env_file)
  - [environment](#environment)
  - [healthcheck](#healthcheck)
  - [hostname](#hostname)
  - [links](#links)
  - [expose](#expose)
  - [logging](#logging)
- [networks](#networks)
  - [driver](#driver)
  - [network_mode](#network_mode)
  - [driver_opts](#driver_opts)
  - [attachable](#attachable)
  - [internal](#internal)
  - [ip definition](#ip definition)
  - [ports](#ports)
  - [secrets](#secrets)
  - [ulimits](#ulimits)
  - [external](#external)
- [volume](#volume)
  - [external](#external)
  - [volumes_from](#volumes_from)

this [Page](https://docs.docker.com/compose/compose-file/)
# Services
### blkio_config
blkio_config defines a set of configuration options to set block IO limits for this service.
> device_read_bps, device_write_bps #containing path and weight
> 
> device_read_iops, device_write_iops #containing path and weight
> 
> weight #Modify the proportion of bandwidth allocated to this service
> 
> weight_device #containing path and weight
> 
> cpu_count,cpu_percent,cpu_shares,cpu_period,cpu_quota,...

### command
command overrides the default command declared by the container image (i.e. by Dockerfile’s CMD).
### config
configs grant access to configs on a per-service basis using the per-service configs configuration. Two different syntax:
1. Short
```yaml
services:
  redis:
    image: redis:latest
    configs:
      - my_config
configs:
  my_config:
    file: ./my_config.txt
  my_other_config:
    external: true
```
- _mount_ **my_config** from _current directory_ to **root path** of _container_.
- the same for **my_other_config** but it has already been defined in the platform.
2. Long
```yaml
services:
  redis:
    image: redis:latest
    configs:
      - source: my_config
        target: /redis_config
        uid: "103"
        gid: "103"
        mode: 0440
configs:
  my_config:
    external: true
  my_other_config:
    external: true
```
### Profiles
Profiles allow to adjust the Compose application model for various usages and environments. A Compose implementation SHOULD allow the user to define a set of active profiles. The exact mechanism is implementation specific and MAY include command line flags, environment variables, etc.
```yaml
services:
  foo:
    image: foo
  bar:
    image: bar
    profiles:
      - test
  baz:
    image: baz
    depends_on:
      - bar
    profiles:
      - test
  zot:
    image: zot
    depends_on:
      - bar
    profiles:
      - debug
```
- to using: `docker compose --profile debug up ...`
### depends_on
depends_on expresses has two type syntax:
1. Short:
```yaml
services:
  web:
    build: .
    depends_on:
      - db
      - redis
  redis:
    image: redis
  db:
    image: postgres
```
2. Long:
```yaml
services:
  web:
    build: .
    depends_on:
      db:
        condition: service_healthy
      redis:
        condition: service_started
  redis:
    image: redis
  db:
    image: postgres
```
which:
- service_started: is an equivalent of the short syntax described above
- service_healthy: specifies that a dependency is expected to be “healthy”
- service_completed_successfully: specifies that a dependency is expected to run to successful completion before ...
### deploy
deploy specifies the configuration for the deployment and lifecycle of services.
#### endpoint_mode
**endpoint_mode** specifies a service discovery method for external clients connecting to a service. Default and available values are platform specific, anyway the Compose specification define two canonical values:
- _endpoint_mode_: vip: Assigns the service a virtual IP (VIP) that acts as the front end for clients to reach the service on a network. Platform routes requests between the client and nodes running the service, without client knowledge of how many nodes are participating in the service or their IP addresses or ports.
- _endpoint_mode_: dnsrr: Platform sets up DNS entries for the service such that a DNS query for the service name returns a list of IP addresses (DNS round-robin), and the client connects directly to one of these.
```yaml
services:
  frontend:
    image: awesome/webapp
    ports:
      - "8080:80"
    deploy:
      mode: replicated
      replicas: 2
      endpoint_mode: vip
```
#### mode
mode define the replication model used to run the service on platform. Either global (exactly one container per physical node) or replicated (a specified number of containers). The default is replicated.
1. global
```yaml
    deploy:
      mode: global
```
2. replicas
```yaml
services:
  frontend:
    image: awesome/webapp
    deploy:
      mode: replicated
      replicas: 6
```
### restart
```yaml
    restart: "no"
    restart: always
    restart: on-failure
    restart: unless-stopped
```
### placement
placement specifies constraints and preferences for platform to select a physical node to run service containers.
#### constraints
defines a REQUIRED property the platform’s node MUST fulfill to run service
```yaml
    deploy:
        placement:
            constraints:
                - disktype=ssd
    or
    deploy:
        placement:
            constraints:
                disktype: ssd
```
#### preferences
defines a property the platform’s node SHOULD fulfill to run service container.
```yaml
    deploy:
        placement:
            preferences:
                - datacenter=us-east
    or
    deploy:
        placement:
            preferences:
                datacenter: us-east
```
### resources
resources configures physical resource constraints for container to run on platform. Those constraints can be configured as a:
- limits: The platform MUST prevent container to allocate more
- reservations: The platform MUST guarantee container can allocate at least the configured amount
```yaml
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 50M
          pids: 1
        reservations:
          cpus: '0.25'
          memory: 20M
```
### restart_policy
configures if and how to restart containers when they exit. If is not set, Compose implementations MUST consider restart field set by service configuration.
condition: One of none, on-failure or any (default: any).
1. delay: How long to wait between restart attempts, specified as a duration (default: 0).
2. max_attempts: How many times to attempt to restart a container before giving up (default: never give up).
  - If the restart does not succeed within the configured window, this attempt doesn’t count toward the configured max_attempts value.
3. window: How long to wait before deciding if a restart has succeeded, specified as a duration (default: decide immediately).
```yaml
deploy:
  restart_policy:
    condition: on-failure
    delay: 5s
    max_attempts: 3
    window: 120s
```
### rollback_config
configures how the service should be rollbacked in case of a failing update.
- parallelism: The number of containers to rollback at a time. If set to 0, all containers rollback simultaneously.
- delay: The time to wait between each container group’s rollback (default 0s).
- failure_action: What to do if a rollback fails. One of continue or pause (default pause)
- monitor: Duration after each task update to monitor for failure (ns|us|ms|s|m|h) (default 0s).
- max_failure_ratio: Failure rate to tolerate during a rollback (default 0).
- order: Order of operations during rollbacks. One of stop-first (old task is stopped before starting new one), or start-first (new task is started first, and the running tasks briefly overlap) (default stop-first).
### update_config
how the service should be updated. Useful for configuring rolling updates.
- parallelism: The number of containers to update at a time.
- delay: The time to wait between updating a group of containers.
- failure_action: What to do if an update fails. One of continue, rollback, or pause (default: pause).
- monitor: Duration after each task update to monitor for failure (ns|us|ms|s|m|h) (default 0s).
- max_failure_ratio: Failure rate to tolerate during an update.
- order: Order of operations during updates. One of stop-first (old task is stopped before starting new one), or start-first (new task is started first, and the running tasks briefly overlap) (default stop-first).
```yaml
deploy:
  update_config:
    parallelism: 2
    delay: 10s
    order: stop-first
```
### devices
defines a list of device mappings for created containers in the form of HOST_PATH:CONTAINER_PATH[:CGROUP_PERMISSIONS].
```yaml
devices:
  - "/dev/ttyUSB0:/dev/ttyUSB0"
  - "/dev/sda:/dev/xvda:rwm"
```
### dns & dns_search
```yaml
dns:
  - 8.8.8.8
  - 9.9.9.9
dns_search:
  - dc1.example.com
  - dc2.example.com
```
### entrypoint
overrides the default entrypoint for the Docker image (i.e. ENTRYPOINT set by Dockerfile).
```yaml
entrypoint:
  - php
  - -d
  - zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20100525/xdebug.so
  - -d
  - memory_limit=-1
  - vendor/bin/phpunit
```
### env_file
```yaml
env_file:
  - ./a.env
  - ./b.env
```
### environment
```yaml
environment:
  RACK_ENV: development
  SHOW: "true"
  USER_INPUT:
```
### healthcheck
declares a check that’s run to determine whether or not containers for this service are “healthy”. This overrides HEALTHCHECK Dockerfile instruction set by the service’s Docker image.
```yaml
healthcheck:
  test: ["CMD", "curl", "-f", "http://localhost"]
  interval: 1m30s
  timeout: 10s
  retries: 3
  start_period: 40s
interval, timeout and start_period are specified as durations.
```
### hostname
### links
links defines a network link to containers in another service. Either specify both the service name and a link alias (SERVICE:ALIAS), or just the service name.
```yaml
web:
  links:
    - db
    - db:database
    - redis
```
### expose
expose defines the ports that Compose implementations MUST expose from container. These ports MUST be accessible to linked services and SHOULD NOT be published to the host machine. Only the internal container ports can be specified.
```yaml
expose:
  - "3000"
  - "8000"
```
### logging
logging defines the logging configuration for the service.
```yaml
logging:
  driver: syslog
  options:
    syslog-address: "tcp://192.168.0.42:123"
```
# networks
Networks are the layer that allow services to communicate with each other.Networks can be created by specifying the network name under a top-level networks section. Services can connect to networks by specifying the network name under the service networks subsection
```yaml
services:
  frontend:
    image: awesome/webapp
    networks:
      - front-tier
      - back-tier

networks:
  front-tier:
  back-tier:
```
### driver
specifies which driver should be used for this network.
Default and available values are platform specific. Compose specification MUST support the following specific drivers: none and host
1. driver: overlay
2. host use the host’s networking stack
```yaml

services:
  web:
    networks:
      hostnet: {}

networks:
  hostnet:
    external: true
    name: host
```
3. none disable networking
```yaml
services:
  web:
    ...
    networks:
      nonet: {}

networks:
  nonet:
    external: true
    name: none
```
### network_mode
set service containers network mode.
```yaml
    network_mode: "none"
    network_mode: "host" #raw access to host’s network interface
    network_mode: "service:[service name]" #access to the specified service only
```
### driver_opts
a list of options as key-value pairs to pass to the driver for this network.These options are driver-dependent
```yaml
driver_opts:
  foo: "bar"
  baz: 1
```
### attachable
If attachable is set to true, then standalone containers SHOULD be able attach to this network, in addition to services.
```yaml
networks:
  mynet1:
    driver: overlay
    attachable: true
```
### internal
By default, Compose implementations MUST provides external connectivity to networks. internal when set to true allow to create an externally isolated network.

### IP definition
```yaml
services:
  frontend:
    image: awesome/webapp
    networks:
      front-tier:
        ipv4_address: 172.16.238.10
        ipv6_address: 2001:3984:3989::10

networks:
  front-tier:
    ipam:
      driver: default
      config:
        - subnet: "172.16.238.0/24"
        - subnet: "2001:3984:3989::/64"
```
### ports
Exposes container ports. Port mapping MUST NOT be used with network_mode: host and doing so MUST result in a runtime error.
1. short syntax
```yaml
ports:
  - "3000"
  - "3000-3005"
  - "8000:8000"
  - "9090-9091:8080-8081"
  - "49100:22"
  - "127.0.0.1:8001:8001"
  - "127.0.0.1:5000-5010:5000-5010"
  - "6060:6060/udp"
```
2. long syntax
```yaml
ports:
  - target: 80
    host_ip: 127.0.0.1
    published: 8080
    protocol: tcp
    mode: host

  - target: 80
    host_ip: 127.0.0.1
    published: 8000-9000
    protocol: tcp
    mode: host
```
### secrets
[secrets](https://docs.docker.com/compose/compose-file/#secrets) grants access to sensitive data defined by secrets on a per-service basis. Two different syntax variants are supported: the short syntax and the long syntax.
### ulimits
```yaml
ulimits:
  nproc: 65535
  nofile:
    soft: 20000
    hard: 40000
```
### external
If _set_ to _true_, external specifies that this network’s lifecycle is maintained outside of that of the application.
If _set_ to _true_, then the resource is not managed by Compose.
If _set_ to _true_ and the network configuration has other attributes set besides name, then Compose Implementations SHOULD reject the Compose file as invalid.
```yaml
services:
  proxy:
    image: awesome/proxy
    networks:
      - outside
      - default
  app:
    image: awesome/app
    networks:
      - default

networks:
  outside:
    external: true
```
# Volume
Volumes are persistent data stores implemented by the platform. 
defines mount host paths or named volumes that MUST be accessible by service containers.
```yaml
services:
  backend:
    image: awesome/database
    volumes:
      - db-data:/etc/data

  backup:
    image: backup-service
    volumes:
      - db-data:/var/lib/backup/data

volumes:
  db-data:
```
it has two type commands:
1. Short syntax
The short syntax uses a single string with colon-separated values to specify a volume mount 
```yaml
VOLUME:CONTAINER_PATH
VOLUME:CONTAINER_PATH:ACCESS_MODE
```
> VOLUME: MAY be either a host path on the platform hosting containers (bind mount) or a volume name
> CONTAINER_PATH: the path in the container where the volume is mounted
> ACCESS_MODE: is a comma-separated , list of options and MAY be set to:
with these options:
  1. _rw_: read and write access (default)
  2. _ro_: read-only access
  3. _z_: SELinux option indicates that the bind mount host content is shared among multiple containers
  4. _Z_: SELinux option indicates that the bind mount host content is private and unshared for other containers
2. Long syntax
The long form syntax allows the configuration of additional fields that can’t be expressed in the short form.
with these options:
1. **type**: the mount type volume, bind, tmpfs or npipe
2. **source**: the source of the mount, a path on the host for a bind mount, or the name of a volume defined in the top-level volumes key. Not applicable for a tmpfs mount.
3. **target**: the path in the container where the volume is mounted
4. **read_only**: flag to set the volume as read-only
5. **bind**: configure additional bind options
6. **propagation**: the propagation mode used for the bind
7. **create_host_path**: create a directory at the source path on host if there is nothing present. Do nothing if there is something present at the path. This is automatically implied by short syntax for backward compatibility with docker-compose legacy.
8. **selinux**: the SELinux re-labeling option z (shared) or Z (private)
9. **volume**: configure additional volume options
10. **nocopy**: flag to disable copying of data from a container when a volume is created
11. **tmpfs**: configure additional tmpfs options
12. **size**: the size for the tmpfs mount in bytes (either numeric or as bytes unit)
13. **mode**: the filemode for the tmpfs mount as Unix permission bits as an octal number
14. **consistency**: the consistency requirements of the mount. Available values are platform specific
example
```yaml
services:
  backend:
    image: awesome/backend
    volumes:
      - type: volume
        source: db-data
        target: /data
        volume:
          nocopy: true
      - type: bind
        source: /var/run/postgres/postgres.sock
        target: /var/run/postgres/postgres.sock

volumes:
  db-data:
```
### external
If set to true, external specifies that this volume already exist on the platform
```yaml
services:
  backend:
    image: awesome/database
    volumes:
      - db-data:/etc/data

volumes:
  db-data:
    external: true
```
### volumes_from
volumes_from mounts all of the volumes from another service or container, optionally specifying read-only access (ro) or read-write (rw). If no access level is specified, then read-write MUST be used.
String value defines another service in the Compose application model to mount volumes from. The _container:_ prefix, if supported, allows to mount volumes from a container that is not managed by the Compose implementation.
```yaml
volumes_from:
  - service_name
  - service_name:ro
  - container:container_name
  - container:container_name:rw
```




