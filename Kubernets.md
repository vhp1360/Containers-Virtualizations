<div dir=rtl>بنام خدا</div>

###### top

- [MiniKube](#minikube)
- [kubectl](#kubctl)
- [Commands series](#command-series)
- [K8S Yaml File parameters explanation](#k8s-yaml-file-parameters-explanation)

# Minikube
- Some Commands
```vala
  minikube start
  
```



[Top](#top)
# Kubectl
- Some Commands
```vala
  kubectl start
  kubctl cluster-info
  kubctl get nodes
  kubctl get nodes --help
  kubectl run ImageName --image=/Path/To/Image:version --port=8080
  kubectl get deployments
  kubectl proxy
  export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
  echo Name of the Pod: $POD_NAME
  curl http://localhost:8001/api/v1/proxy/namespaces/default/pods/$POD_NAME/
  

```





[Top](#top)
# Commands series
```dart
minikube start --vm-driver=DriverName
kubectl config current-context
kubectl create -f path/2/file.yml
kubectl get -f path/2/file.yml
kubectl egt pods
kubectl get services|svc|deployments [-l lblKey=lblValue{|,...}|--show-lables]
kubectl get rs

---
#finding Service port
PORT=$(kubectl get svc ServiceName -o jsonpath="{.spec.ports[0].nodePort}")
IP=$(minikube ip)
---
kubectl get endpoint serviceName -o yaml
kubectl describe namespace nameSpaceName
kubectl Command --record
# to Find all URLs
POD_NAME=$(kubectl get pod --no-headers -o=custom-columns=NAME:.metadata.name -l type=api,service=go-demo-2 | tail -1)
kubectl exec $POD_NAME env
---
#Update 
kubectl set -f File.yaml ...
kubectl edit -f File.yaml       # it is not recommended
kubectl rollout status|history -w -f File.yaml  # to check what would be happened during upgraded
kubectl rollout undo -f File.yaml [--to-revision=No.]
kubectl rollout pause|resume -f File.yaml
---
kubectl scale deployments File.yaml --replicas No. [--record]


```
[Top](#top)
### rollout
rollingupdate allows you to update pods gradually with many offers:
  
  - the most important is _spec.strategy.type_:
  
    1. RollingUpdate: new pods are added gradually and old ones are terminated
   
    2. All old pods are terminated before any new ones are added.
    
    - it has to options:
      - maxSurge: The number of pods that can be created above the desired amount of pods during an update
      - maxUnavailable: The number of pods that can be unavailable during the update process


[Top](#top)
# K8S Yaml File parameters explanation
`minReadySeconds`: minimum seconds K8S waits, before start to check Pod's healthy.

`readinessProbe`: specifies the way to check is Pod ready or not? it is done by K8S.

`livenessProbe`: sepecifies the way for K8S to check is Pod still Up or not.

`revisionHistoryLimit`: how many previous version of Pod must be keep by K8S.

`strategy`: specefies way of updating the Pod.there are 2 ways:
  - `recreate`: useful for apps which are not defined for up mor than one instance at time.
  - `rollingupdate`: it is default option and allow us to upgrade without downtime.it has two parameters:
    - `maxSurge`: The number of pods that can be created above the desired amount of pods during an update
    - `maxUnavailable` : The number of pods that can be unavailable during the update process 


[Top](#top)
## Ingress

```dart
minikube addons enable ingress # enable it
kubectl get pods -n kube-system | grep ingress #2check is it up 
curl -i "htpp://$IP/healthz"


[Top](#top)
#
